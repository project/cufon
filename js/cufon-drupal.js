
// Initialize Cufon based on Drupal settings
for (o in Drupal.settings.cufonSelectors) { 
  s = Drupal.settings.cufonSelectors[o];
  Cufon.replace(s.selector, s.options)
}

// Work around Internet Explorer rendering delay
Cufon.now();
