<?php
/**
 * @file
 * Adds simple Cufón support to Drupal. 
 *
 * Please read README.txt for installation and configuration instructions.
 */

/**
 * Implementation of hook_perm().
 */
function cufon_perm() {
  return array('administer cufon');
}

/**
 * Implementation of hook_menu().
 */
function cufon_menu() {
  $items = array();
  $items['admin/settings/cufon'] = array(
    'title' => 'Cufón Settings',
    'description' => 'Adds cufon support to Drupal',
    'access arguments' => array('administer cufon'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cufon_admin'),
    'file' => 'cufon.admin.inc',
  );
  return $items;
}

/**
 * _cufon_discover_fonts().
 *
 * Discover fonts according to the simple algorithm laid out at the top of
 * this file.  
 */
function _cufon_discover_fonts() {
  $fonts = array();
  $pattern = '.*\.font\.js$';
  $files = file_scan_directory('./sites/all/libraries/cufon-fonts', $pattern);
  $files += file_scan_directory('./'. conf_path() .'/libraries/cufon-fonts', $pattern);
  $files += file_scan_directory('./'. path_to_theme(), $pattern);
  foreach ($files as $filename => $file) {
    // Strip off './'
    $filename = substr($file->filename, 2);
    // Resolve font family
    $fonts[$filename] = _cufon_get_font_family($file->basename);
  }
  return $fonts;
}

/**
 * _cufon_resolve_font_familes().
 *
 * Takes a list of files, such as the one returned by file_scan_directory, and 
 * returns an array of font file -> font family name values.
 */
function _cufon_get_font_family($filename) {

  $family = str_replace('.font.js', '', $filename);

  // Split on hyphen
  if (($hyphen_pos = strpos($family, '-')) && ($hyphen_pos !== FALSE)) {
    $family = substr($family, 0, $hyphen_pos);
  }

  // Split on last underscore and see if its numeric
  $suffix_pos = strrpos($family, '_');
  $suffix = substr($family, $suffix_pos + 1);
  if (is_numeric($suffix)) {
    $family = substr($family, 0, $suffix_pos);
  }

  // Remove remaining hyphens and add to our array
  $family = str_replace('_', ' ', $family);

  return $family;
}

/**
 * Implementation of hook_theme_registry_alter().
 */
function cufon_theme_registry_alter(&$theme_registry) {
  if (isset($theme_registry['page'])) {
    if (!empty($theme_registry['page']['preprocess functions'])) {
      if ($key = array_search('cufon_preprocess_page', $theme_registry['page']['preprocess functions'])) {
        unset($theme_registry['page']['preprocess functions'][$key]);
      }
    }
    // Run before js is added before template_preprocess_page().
    array_unshift($theme_registry['page']['preprocess functions'], 'cufon_preprocess_page');
  }
}


/**
 * Implementation of hook_preprocess_page().
 */
function cufon_preprocess_page(&$vars) {
  foreach (_cufon_discover_fonts() as $filename => $name) {
    drupal_add_js($filename, 'module');
  }
  $selectors = variable_get('cufon_selectors', array());
  drupal_add_js(array('cufonSelectors' => $selectors), 'setting');
  $js =  dirname(__FILE__) .'/js';
  drupal_add_js($js .'/cufon-yui.js', 'module');
  drupal_add_js($js .'/cufon-drupal.js', 'module', 'footer');
}
